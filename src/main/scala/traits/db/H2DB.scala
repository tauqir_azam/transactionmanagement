package traits.db

import java.sql._
import java.math.BigDecimal

import org.h2.jdbc.JdbcSQLException
import request.User
import traits.config.Configuration



/**
  * Created by andrew on 8/20/17.
  */
trait H2DB extends Configuration{

  Class.forName(config.getString("db.h2database.driver"))
  var connection: Connection = DriverManager.getConnection(config.getString("db.h2database.connection"),
                                                           config.getString("db.h2database.user"),
                                                           config.getString("db.h2database.password"))
  connection.setAutoCommit(false)
  createUserTable()
  createTransactionsTable()


  def createUserTable(): Boolean = {
    val statement = connection.createStatement
    val result = statement.execute(CREATE_USER_TABLE)
    connection.commit()
    result
  }

  def createTransactionsTable(): Boolean = {
    val statement = connection.createStatement
    val result = statement.execute(CREATE_TRANSACTION_TABLE)
    connection.commit()
    result
  }

  def addUser(user: User): Either[String, Boolean] = {
    val statement = connection.prepareStatement(INSERT_NEW_USER)
    statement.setString(1, user.id)
    statement.setString(2, user.name)
    statement.setBigDecimal(3, new BigDecimal(user.balance.doubleValue()))
    try {
      val result = statement.executeUpdate()
      connection.commit()
      Right(true)
    } catch {
      case jse: JdbcSQLException => {
        connection.rollback()
        Left(jse.getOriginalMessage) }
      case se: Exception => {
        connection.rollback()
        Left(se.getMessage) }
    }
  }


  def getUserById(id: String): Either[String, User] = {
    try {
      val selectQuery = connection.createStatement
      val result = selectQuery.executeQuery(String.format(SELECT_ONE_USER, id))
      connection.commit()
      if(!result.first()) return Left("User with provided Id wasn't found")
      Right(User(result.getString(1), result.getString(2), result.getBigDecimal(3)))
    } catch {
      case jse: JdbcSQLException => {
        connection.rollback()
        Left(jse.getOriginalMessage) }
      case se: Exception => {
        connection.rollback()
        Left(se.getMessage) }
    }
  }


  def moneyTransfer(from: String, to: String, amount: BigDecimal): Either[String, Boolean]  = {
    try {
      val decreasePrepStatement = connection.prepareStatement(DECREASE_BALANCE)
      decreasePrepStatement.setBigDecimal(1, amount)
      decreasePrepStatement.setString(2, from)
      decreasePrepStatement.executeUpdate
      val increasePrepStatement = connection.prepareStatement(INCREASE_BALANCE)
      increasePrepStatement.setBigDecimal(1, amount)
      increasePrepStatement.setString(2, to)
      increasePrepStatement.executeUpdate
      connection.commit()
      Right(true)
    } catch {

      case jse: JdbcSQLException => {
        connection.rollback()
        Left(jse.getOriginalMessage) }
      case se: Exception => {
        connection.rollback()
        Left(se.getMessage) }
    }
  }
}
