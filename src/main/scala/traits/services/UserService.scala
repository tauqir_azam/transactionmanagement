package traits.services

import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.StatusCodes.{BadRequest, OK}
import request.User
import responces.FailureResponce
import traits.db.H2DB
import java.math.BigDecimal

/**
  * Created by andrew on 8/20/17.
  */
trait UserService extends H2DB {

  def createUser(user : User): Either[String, Boolean] = {
    if(getUserById(user.id).isRight) return Left("User with this id already exists!")
    if(user.balance < 0) return Left("Balance can't be negative!")
    addUser(user)
  }
  def getUser(id: String): Either[String, User] = {
    getUserById(id)
  }

}
