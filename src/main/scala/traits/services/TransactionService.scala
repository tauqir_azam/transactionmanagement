package traits.services

import traits.db.H2DB

/**
  * Created by andrew on 8/20/17.
  */
trait TransactionService extends H2DB {
  def transferMoney(from: String, to: String, amount: BigDecimal): Either[String, Boolean] = {
    return getUserById(from) match {
      case Right(user) => {
        if(user.balance>amount) moneyTransfer(from, to, new java.math.BigDecimal(amount.doubleValue())) else Left("User have not enough balance for transaction")
      }
      case Left(message) => Left(message)
    }
  }
}
