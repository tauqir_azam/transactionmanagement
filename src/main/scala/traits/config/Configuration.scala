package traits.config

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import request.{Transaction, User}
import responces.{FailureResponce, SucessResponce}
import spray.json.DefaultJsonProtocol

/**
  * Created by andrew on 5/16/17.
  */
trait Configuration extends Protocols with Constants{

  implicit val system = ActorSystem("microServiceSystem")
  implicit val materializer = ActorMaterializer()
  implicit val executor = system.dispatcher

  val config = ConfigFactory.load()
  val logger = Logging(system, "my.nice.string")
}


trait Protocols extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val userRequestFormat = jsonFormat3(User.apply)
  implicit val transactionFormat = jsonFormat3(Transaction.apply)
  implicit val sucessFormat = jsonFormat1(SucessResponce.apply)
  implicit val failFormat = jsonFormat2(FailureResponce.apply)
}

trait Constants {

  val CREATE_USER_TABLE = "CREATE TABLE USER\n" +
    "(ID CHAR(100) PRIMARY KEY,\n" +
    "NAME CHAR(25) NOT NULL,\n" +
    "BALANCE DECIMAL(20, 2))"
  val CREATE_TRANSACTION_TABLE = "CREATE TABLE TRANSACTIONS (\n" +
    "  ID BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,\n" +
    "  FROM_USER VARCHAR(40) NOT NULL,\n" +
    "  TO_USER VARCHAR(40) NOT NULL,\n" +
    "  DATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
    "  AMOUNT DECIMAL(20, 2) NOT NULL)"
  val INSERT_NEW_USER = "INSERT INTO USER (id, name, balance) values (?, ?, ?)"
  val SELECT_ONE_USER = "SELECT * FROM USER WHERE ID = '%s'"
  val DECREASE_BALANCE = "UPDATE USER SET BALANCE = BALANCE - ? WHERE ID = ?"
  val INCREASE_BALANCE = "UPDATE USER SET BALANCE = BALANCE + ? WHERE ID = ?"

  val failed = "Failed"
  val success = "Success"
}