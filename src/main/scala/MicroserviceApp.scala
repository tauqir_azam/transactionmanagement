import java.util.Scanner

import akka.http.scaladsl.Http
import routers.TransactionRouter

import scala.io.StdIn


/**
  * Created by andrew on 5/13/17.
  */

object MicroserviceApp extends TransactionRouter {

  def main(args: Array[String]): Unit = {

      val bindingFuture = Http().bindAndHandle(routes, config.getString("http.interface"), config.getInt("http.port"))
      println(s"Server online at http://${config.getString("http.interface")}:${config.getInt("http.port")}")
      println("Press RETURN to stop...")
      StdIn.readLine()
      bindingFuture
        .flatMap(_.unbind())
        .onComplete(_ => {
          system.terminate()
        })
  }
}