package routers

import akka.http.scaladsl.model.StatusCodes.{BadRequest, OK}
import akka.http.scaladsl.server.Directives._
import request.{Transaction, User}
import responces.{FailureResponce, SucessResponce}
import spray.json._
import traits.config.{Configuration, Protocols}
import traits.services.{TransactionService, UserService}


/**
  * Created by andrew on 5/16/17.
  */
trait TransactionRouter extends UserService with TransactionService with Configuration with Protocols {

  //  val routes = {
  //    logRequestResult("akka-http-microservice") {
  //      path("user" / JavaUUID) { id =>
  //        (get) {
  //              complete {
  //                getUser(id.toString) match {
  //                  case Right(responce) => OK -> responce.toJson
  //                  case Left(errorMessage) => BadRequest -> FailureResponce(failed,errorMessage).toJson
  //                }
  //              }
  //            } ~
  //            (post & entity(as[User])) { user =>
  //              complete {
  //                createUser(user) match {
  //                  case Right(responce) => OK -> SucessResponce("Success")
  //                  case Left(errorMessage) => BadRequest -> FailureResponce(failed,errorMessage).toJson
  //                }
  //              }
  //            }
  //      } ~
  //      path("transaction") {
  //        (post & entity(as[Transaction])) { transaction =>
  //          complete {
  //            transferMoney(transaction.fromUser, transaction.toUser, transaction.amount) match {
  //              case Right(responce) => OK -> SucessResponce("Success")
  //              case Left(errorMessage) => BadRequest -> FailureResponce(failed,errorMessage).toJson
  //            }
  //          }}
  //      }
  //    }
  //  }


  val routes =
    logRequestResult("akka-http-microservice") {
        (path("user" / JavaUUID ) & get) { id =>
          complete {
            getUser(id.toString) match {
              case Right(responce) => OK -> responce.toJson
              case Left(errorMessage) => BadRequest -> FailureResponce(failed, errorMessage).toJson
            }
          }
        } ~
        (path("user") & post & entity(as[User])) { user =>
          complete {
            createUser(user) match {
              case Right(responce) => OK -> SucessResponce(success)
              case Left(errorMessage) => BadRequest -> FailureResponce(failed, errorMessage).toJson
            }
          }
        } ~
        (path("transaction") & post & entity(as[Transaction])) { transaction =>
          complete {
            transferMoney(transaction.fromUser, transaction.toUser, transaction.amount) match {
              case Right(responce) => OK -> SucessResponce(success)
              case Left(errorMessage) => BadRequest -> FailureResponce(failed, errorMessage).toJson
            }
          }
        }
    }




}
