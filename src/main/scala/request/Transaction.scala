package request

/**
  * Created by andrew on 8/20/17.
  */
case class Transaction(fromUser: String, toUser: String, amount: BigDecimal)
