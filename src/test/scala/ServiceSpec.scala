import akka.event.NoLogging
import akka.http.scaladsl.model.ContentTypes._
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.stream.scaladsl.Flow
import com.mongodb.client.result.UpdateResult._
import org.mongodb.scala.result.UpdateResult
import org.scalatest.{FlatSpec, _}
import request.{Transaction, User}
import responces.{FailureResponce, SucessResponce}
import routers.TransactionRouter
import spray.json._
import traits.config.{Constants, Protocols}

import scala.concurrent.Future


object TestApplication extends TransactionRouter {
  val testUser = User("83e538a4-8587-11e7-bb31-be2e44b06b34", "mr.Test", BigDecimal(100.00))
  val testUserNegativeBalance = User("f4d80ade-3d69-4389-9cff-07a18ad32696", "mr.Test", BigDecimal(-100.00))
  val testUserPositiveBalance = User("f4d80ade-3d69-4389-9cff-07a18ad32696", "mr.Test", BigDecimal(100.00))
  val failedTransaction = Transaction("83e538a4-8587-11e7-bb31-be2e44b06b34","f4d80ade-3d69-4389-9cff-07a18ad32696", 10000.00)
  val successTransaction = Transaction("83e538a4-8587-11e7-bb31-be2e44b06b34","f4d80ade-3d69-4389-9cff-07a18ad32696", 10.00)
  val notExistingUserTransaction = Transaction("83e538a4-8587-1111-bb31-be2e44b06b34","f4d80ade-3d69-1111-9cff-07a18ad32696", 10.00)
}

/**
  * Created by andrew on 5/16/17.
  */
class ServiceSpec extends FlatSpec with Matchers with ScalatestRouteTest with Constants with Protocols {

  override def testConfigSource = "akka.loglevel = WARNING"
  val config = testConfig
  val logger = NoLogging
  val app = TestApplication
  val routes = app.routes

  "User Service" should "respond success to single valid POST query" in {

    Post(s"/user", app.testUser) ~> routes ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[SucessResponce] shouldBe SucessResponce(success)
    }
  }

  it should "respond to single GET query with created user info" in {
    Get(s"/user/83e538a4-8587-11e7-bb31-be2e44b06b34") ~> routes ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[User] shouldBe app.testUser
    }
  }


  it should "respond to single GET query with not existing user" in {
    Get(s"/user/ed1bea17-9ad4-42e3-93d1-f2560239357f") ~> routes ~> check {
      status shouldBe BadRequest
      contentType shouldBe `application/json`
      responseAs[FailureResponce] shouldBe FailureResponce(failed, "User with provided Id wasn't found")
    }
  }

  it should "respond to single POST query with negative balance" in {
    Post(s"/user", app.testUserNegativeBalance) ~> routes ~> check {
      status shouldBe BadRequest
      contentType shouldBe `application/json`
      responseAs[FailureResponce] shouldBe FailureResponce(failed, "Balance can't be negative!")
    }
  }

  it should "respond to single POST query with positive balance" in {
    Post(s"/user", app.testUserPositiveBalance) ~> routes ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[SucessResponce] shouldBe SucessResponce(success)
    }
  }


  it should "respond to single POST query existing id" in {
    Post(s"/user", app.testUser) ~> routes ~> check {
      status shouldBe BadRequest
      contentType shouldBe `application/json`
      responseAs[FailureResponce] shouldBe FailureResponce(failed, "User with this id already exists!")
    }
  }

  "Transcation Service" should "respond fail to single invalid POST query if trasaction bigger that user balance" in {
      Post(s"/transaction", app.failedTransaction) ~> routes ~> check {
        status shouldBe BadRequest
        contentType shouldBe `application/json`
        responseAs[FailureResponce] shouldBe FailureResponce(failed, "User have not enough balance for transaction")
      }
    }

  it should "respond to single POST query with sucess if transaction is valid" in {
    Post(s"/transaction", app.successTransaction) ~> routes ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[SucessResponce] shouldBe SucessResponce(success)
    }
  }

  it should "respond reduce the amount after transaction for first user" in {
    Get(s"/user/83e538a4-8587-11e7-bb31-be2e44b06b34") ~> routes ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[User].balance shouldBe BigDecimal(90.00)
    }
  }

  it should "respond increase the amount after transaction for second user" in {
    Get(s"/user/f4d80ade-3d69-4389-9cff-07a18ad32696") ~> routes ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[User].balance shouldBe BigDecimal(110.00)
    }
  }

  it should "respond to single POST query with sucess if transaction inolves not existing user" in {
    Post(s"/transaction", app.notExistingUserTransaction) ~> routes ~> check {
      status shouldBe BadRequest
      contentType shouldBe `application/json`
      responseAs[FailureResponce] shouldBe FailureResponce(failed, "User with provided Id wasn't found")
    }
  }

}
