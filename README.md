# Transaction Users
Implemented and ready for use

### Prerequisites

* Please make sure you have latest Scala installed on your environment!

* Please make sure you have latest SBT installed on your environment!

* Use next file to configure the service
> resources/application.conf 


### Implementation Details


1) Solution using Scala and Akka Frameworks
2) Solution Using H2Database for in-memory Storage

### Running

When you have your SBT installed please in terminal open Project folder
and execute next command in command line:

```
sbt run

```

To stop application just press RETURN



Now you should be able to execute rest requests, while list examples of possible requests are:

To create new USER:
```
curl -X POST \
  http://0.0.0.0:9000/user \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 84e61182-083b-3e12-3554-b703dab7da1b' \
  -d ' {
        "id": "83e538a4-8587-11e7-bb31-be2e44b06b34",
        "name": "mr. Shit",
        "balance": 1000.00
 }'
```

To get USER info:
```
curl -X GET \
  http://0.0.0.0:9000/user/83e538a4-8587-11e7-bb31-be2e44b06b34 \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: b05d267c-0017-9cc4-1546-ee0a98a2275f' \
  -d ' {
        "id": "83e538a4-8587-11e7-bb31-be2e44b06b34",
        "name": "mr. Shit",
        "balance": 123444.44
 }'
```
To make a transaction happen:
```
curl -X POST \
  http://0.0.0.0:9000/transaction \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 4ee5f22e-badb-8d01-7ffe-2fec1a7fd505' \
  -d ' {
        "fromUser": "83e538a4-8587-11e7-bb31-be2e44b06b34",
        "toUser": "f4d80ade-3d69-4389-9cff-07a18ad32696",
        "amount": 105
 } '
```

#### Notes

* In case of providing wrong id you will have corresponding warnings
* In case user not exists or similar situations you will have corresponding warnings

### Testing

In order to run unit testing please execute next command:

```
sbt test

```