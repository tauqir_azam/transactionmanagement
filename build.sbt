name := "MivieTicketMicroservice"

version := "1.0"

scalaVersion := "2.12.2"

libraryDependencies += "com.typesafe.akka" % "akka-actor_2.12" % "2.5.0"
libraryDependencies += "com.typesafe.akka" % "akka-stream_2.12" % "2.5.1"
libraryDependencies += "com.typesafe.akka" % "akka-http_2.12" % "10.0.6"
libraryDependencies += "org.scalatest" % "scalatest_2.12" % "3.2.0-SNAP6"
libraryDependencies += "com.typesafe.akka" % "akka-testkit_2.12" % "2.5.1"
libraryDependencies += "com.typesafe.akka" % "akka-http-testkit_2.12" % "10.0.6"
libraryDependencies += "com.typesafe.akka" % "akka-http-spray-json_2.12" % "10.0.6"
libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "2.0.0"
libraryDependencies += "com.h2database" % "h2" % "1.4.196"

        